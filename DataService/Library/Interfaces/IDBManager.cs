﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataService.Library.Interfaces
{
    interface IDBManager
    {

        void writeMeeting(Meeting newMeeting);

        void writeContact(Contact newContact);

        List<Meeting> getAllMeetings();

        List<Contact> getAllContacts();

    }
}
