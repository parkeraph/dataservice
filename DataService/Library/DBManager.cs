﻿using DataService.Library.Interfaces;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using CsvHelper;
using System.Globalization;
using System.Web.Hosting;

namespace DataService.Library
{
    public class DBManager : IDBManager
    {
        private readonly string _meetingXMLPath;
        private readonly string _contactXMLPath;

        public DBManager()
        {
            var mpath = ConfigurationManager.AppSettings["MeetingsTableFile"];
            var cpath = ConfigurationManager.AppSettings["ContactTableFile"];

            _meetingXMLPath = Path.Combine(HostingEnvironment.ApplicationPhysicalPath, "App_Data", mpath);
            _contactXMLPath = Path.Combine(HostingEnvironment.ApplicationPhysicalPath, "App_Data", cpath);
        }

        public void writeMeeting(Meeting newMeeting)
        {

            var state = getAllMeetings().ToArray();
            int nextID = state[state.Length-1].ID + 1;
            newMeeting.ID = nextID;

            using (var st = File.Open(_meetingXMLPath, FileMode.Append))
            {
                using (StreamWriter sw = new StreamWriter(st))
                {
                    using (CsvWriter csv = new CsvWriter(sw, CultureInfo.InvariantCulture))
                    {
                        csv.Configuration.HasHeaderRecord = false;
                        csv.WriteRecord<Meeting>(newMeeting);
                        sw.Write("\r\n");
                    }
                }
            }
        }

        public void writeContact(Contact newContact)
        {

            var state = getAllContacts().ToArray();
            int nextID = state[state.Length-1].ID + 1;
            newContact.ID = nextID;

            using (var st = File.Open(_contactXMLPath, FileMode.Append))
            {
                using (StreamWriter sw = new StreamWriter(st))
                {
                    using (CsvWriter cw = new CsvWriter(sw, CultureInfo.InvariantCulture))
                    {
                        cw.Configuration.HasHeaderRecord = false;
                        cw.WriteRecord<Contact>(newContact);
                        sw.Write("\r\n");
                    }
                }
            }
        }

        public List<Meeting> getAllMeetings()
        {
            IEnumerable<Meeting> meetings;
            List<Meeting> returnVal = new List<Meeting>();
            using (StreamReader sr = new StreamReader(_meetingXMLPath))
            {
                using (CsvReader cr = new CsvReader(sr, CultureInfo.InvariantCulture))
                {
                   returnVal = cr.GetRecords<Meeting>().ToList();
                }
            }

            return returnVal;
        }

        public List<Contact> getAllContacts()
        {
            IEnumerable<Contact> contacts;
            List<Contact> returnVal = new List<Contact>();
            using (StreamReader sr = new StreamReader(_contactXMLPath))
            {
                using (CsvReader cr = new CsvReader(sr, CultureInfo.InvariantCulture))
                {
                    returnVal = cr.GetRecords<Contact>().ToList();
                }
            }

            return returnVal;
        }

      

       

        
    }
}