﻿using DataService.Library;
using DataService.Library.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace DataService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "DataService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select DataService.svc or DataService.svc.cs at the Solution Explorer and start debugging.
    public class DataService : IDataService
    {
        IDBManager DBManager = new DBManager();

        public void newMeeting(Meeting newMeeting)
        {
            DBManager.writeMeeting(newMeeting);
        }

        public void newContact(Contact newContact)
        {
            DBManager.writeContact(newContact);
        }

        public Meeting[] getAllMeetings()
        {
            var meetingsList = DBManager.getAllMeetings();
            return meetingsList.ToArray();
        }

        public Contact[] getAllContacts()
        {
            var contactsList = DBManager.getAllContacts();
            return contactsList.ToArray();
        }
    }
}
