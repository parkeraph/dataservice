﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace DataService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IDataService" in both code and config file together.
    [ServiceContract]
    public interface IDataService
    {
        [OperationContract]
        void newMeeting(Meeting newMeeting);

        [OperationContract]
        void newContact(Contact newContract);

        [OperationContract]
        Meeting[] getAllMeetings();

        [OperationContract]
        Contact[] getAllContacts();
        


    }

    //define the data structure in the XML files
    //members with question mark types are nullable, they can be saved as null
    //strings can always just be left as blank

    [Serializable()]
    [XmlRoot(ElementName ="meeting")]
    [DataContract]
    public class Meeting
    {
        [XmlAttribute("ID")]
        [DataMember]
        public int ID { get; set; }

        [XmlAttribute("title")]
        [DataMember]
        public string title { get; set; }

        [XmlAttribute("dateTime")]
        [DataMember]
        public DateTime dateTime { get; set; }

        [XmlAttribute("contactID")]
        [DataMember]
        public int contactID { get; set; }

        [XmlAttribute("zip")]
        [DataMember]
        public int zip { get; set; }

        [XmlAttribute("lat")]
        [DataMember]
        public decimal lat { get; set; }

        [XmlAttribute("long")]
        [DataMember]
        public decimal lon { get; set; }

    }
    
    [Serializable()]
    [XmlRoot(ElementName = "contract")]
    [DataContract]
    public class Contact
    {

        [XmlAttribute("ID")]
        [DataMember]
        public int ID { get; set; }

        [XmlAttribute("firstName")]
        [DataMember]
        public string firstName { get; set; }

        [XmlAttribute("lastName")]
        [DataMember]
        public string lastName { get; set; }

        [XmlAttribute("phone")]
        [DataMember]
        public long phone { get; set; }

        [XmlAttribute("email")]
        [DataMember]
        public string email { get; set; }
    }
}
